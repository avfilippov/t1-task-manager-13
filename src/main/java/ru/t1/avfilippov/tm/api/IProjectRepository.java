package ru.t1.avfilippov.tm.api;

import ru.t1.avfilippov.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    Project add(Project project);

    List<Project> findAll();

    void clear();

    boolean existsById(String id);

    Project create(String name, String description);

    Project create(String name);

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project remove(Project project);

    Project removeById(String id);

    Project removeByIndex(Integer index);

}
