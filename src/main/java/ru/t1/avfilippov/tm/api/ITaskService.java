package ru.t1.avfilippov.tm.api;

import ru.t1.avfilippov.tm.enumerated.Status;
import ru.t1.avfilippov.tm.model.Task;

import java.util.List;

public interface ITaskService {

    Task create(String name, String description);

    Task add(Task task);

    List<Task> findAll();

    List<Task> findAllByProjectId(String projectId);

    void clear();

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    Task remove(Task task);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    Task changeTaskStatusById(String id, Status status);

    Task changeTaskStatusByIndex(Integer index, Status status);

}
