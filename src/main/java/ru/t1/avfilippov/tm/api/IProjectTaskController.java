package ru.t1.avfilippov.tm.api;

public interface IProjectTaskController {

    void bindTaskToProject();

    void unbindTaskFromProject();

}
